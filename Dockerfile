FROM node:latest

RUN curl -o- -L https://yarnpkg.com/install.sh | bash
RUN npm i -g typescript ts-node
RUN npm i -g nodemon

ADD . /var/www/curses-api

WORKDIR /var/www/curses-api

RUN yarn install

EXPOSE 3000

CMD ["docker/start.sh"]